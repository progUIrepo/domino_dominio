import domain.EstadoPedidoCancelado
import domain.EstadoPedidoEnViaje
import domain.EstadoPedidoEntregado
import domain.EstadoPedidoListoParaEnviar
import domain.EstadoPedidoListoParaRetirar
import domain.EstadoPedidoPreparando
import domain.NoPuedeAgregarPlatoException
import domain.NoSePuedeCambiarDeEstadoException
import domain.Pedido
import domain.Plato
import java.time.LocalDateTime
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

import static org.junit.Assert.*
import static org.mockito.Mockito.when
import domain.Cliente
import org.uqbar.commons.model.exceptions.UserException

class PedidoTest {
	
	Pedido pedido
	@Mock
	Plato plato  = Mockito.mock(Plato)
	Cliente cliente = new Cliente
	
    @Before
    def void setUp() {
		cliente.nombre = "Rodolfo"
	    pedido = new Pedido(cliente)
    } 
	
	@Test
	def unPedidoNuevoTieneEstadoPreparando() {
		assertEquals(pedido.estado.class, EstadoPedidoPreparando)
	}
	
	@Test
	def unPedidoEnEstadoPreparandoPermiteAgregarUnPlato() {
	    assertTrue(pedido.puedeAgregarPlato)
	}
	
    @Test
    def unPedidoEnEstadoPreparandoNoEsParaDelivery() {
    	assertFalse(pedido.esDelivery)
    }
    
    @Test
    def unPedidoEnEstadoPreparandoEsUnPedidoAbierto() {
    	assertTrue(pedido.esAbierto)
    	assertFalse(pedido.esCerrado)
    }
    
    @Test
    def unPedidoEnEstadoPreparandoPuedeSerSeteadoADelivery() {
    	pedido.toggleDelivery
    	assertTrue(pedido.esDelivery)
    }
	
	@Test
	def unPedidoCambiaDeEstadosSatisfactoriamenteSiNoEsParaDelivery() {
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoListoParaRetirar)
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoEntregado)
	}
	
	@Test
	def unPedidoListoParaRetirarSeConsideraUnPedidoAbierto() {
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoListoParaRetirar)
    	assertTrue(pedido.esAbierto)
    	assertFalse(pedido.esCerrado)
	}
	
	@Test
	def unPedidoEntregadoSeConsideraUnPedidoCerrado() {
		pedido.siguienteEstado
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoEntregado)
    	assertTrue(pedido.esCerrado)
    	assertFalse(pedido.esAbierto)
	}
	
	@Test
	def unPedidoEnEstadoPreparandoPuedeCancelarse() {
		pedido.cancelar
		assertEquals(pedido.estado.class, EstadoPedidoCancelado)
	}
	
	@Test
	def unPedidoCanceladoEsUnPedidoCerrado() {
		pedido.cancelar
		assertEquals(pedido.estado.class, EstadoPedidoCancelado)
    	assertTrue(pedido.esCerrado)
    	assertFalse(pedido.esAbierto)
	}
	
	@Test
	def unPedidoEnEstadoListoParaRetirarEsCancelable() {
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoListoParaRetirar)
		pedido.cancelar
		assertEquals(pedido.estado.class, EstadoPedidoCancelado)
	}
	
	@Test
	def unPedidoCambiaDeEstadosSatisfactoriamenteSiEsParaDelivery() {
		pedido.toggleDelivery
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoListoParaEnviar)
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoEnViaje)
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoEntregado)
	}
 	
 	@Test
	def unPedidoEnEstadoListoParaEnviarEsCancelable() {
		pedido.toggleDelivery
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoListoParaEnviar)
		pedido.cancelar
		assertEquals(pedido.estado.class, EstadoPedidoCancelado)
	}   
	 
	@Test
	def unPedidoListoParaEnviarEsAbierto() {
		pedido.toggleDelivery
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoListoParaEnviar)
    	assertTrue(pedido.esAbierto)
    	assertFalse(pedido.esCerrado)
	}   
	 	
 	@Test
	def unPedidoEnEstadoEnViajeEsCancelable() {
		pedido.toggleDelivery
		pedido.siguienteEstado
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoEnViaje)
		pedido.cancelar
		assertEquals(pedido.estado.class, EstadoPedidoCancelado)
	} 
	 
	@Test
	def unPedidoEnViajeEsAbierto() {
		pedido.toggleDelivery
		pedido.siguienteEstado
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoEnViaje)
    	assertTrue(pedido.esAbierto)
    	assertFalse(pedido.esCerrado)
	}   
	 	
	@Test
	def unPedidoEnEstadoCanceladoNoPuedeCancelarseNuevamenteYLanzaExcepcion() {
		pedido.cancelar
		assertEquals(pedido.estado.class, EstadoPedidoCancelado)
	    try {
			pedido.cancelar
			fail("Se espera que falle por estar cancelado previamente") 
		}
		catch(UserException e) {
			assertEquals(e.getMessage(), "El pedido fue cancelado. Si lo desea puede hacer un nuevo pedido")
		}
//		catch(NoSePuedeCambiarDeEstadoException e) {
//			assertEquals(e.getMessage(), "El pedido fue cancelado. Si lo desea puede hacer un nuevo pedido")
//		}
	}
	
	@Test
	def unPedidoEnEstadoEntregadoNoPuedeCancelarseYLanzaExcepcion() {
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoListoParaRetirar)
		pedido.siguienteEstado
		assertEquals(pedido.estado.class, EstadoPedidoEntregado) try { pedido.cancelar fail("Se espera que falle por estar entregado previamente") 
		}
		catch(UserException e) {
			assertEquals(e.getMessage(), "El pedido fue entregado. Si lo desea puede hacer un nuevo pedido")
		}
//		catch(NoSePuedeCambiarDeEstadoException e) {
//			assertEquals(e.getMessage(), "El pedido fue entregado. Si lo desea puede hacer un nuevo pedido")
//		}
	}
		
	@Test
	def unPedidoEnEstadoPreparandoNoPuedeIrAUnEstadoAnteriorYLanzaExcepcion() {
		assertEquals(pedido.estado.class, EstadoPedidoPreparando)
	    try {
			pedido.anteriorEstado
			fail("Se espera que falle por no tener estado previo") 
		}
		catch(UserException e) {
			assertEquals(e.getMessage(), "Un pedido que se esta preparando no tiene un estado anterior")
		}
//		catch(NoSePuedeCambiarDeEstadoException e) {
//			assertEquals(e.getMessage(), "Un pedido que se esta preparando no tiene un estado anterior")
//		}
	}
	
	@Test
	def unPedidoEnEstadoPreparandoAgregaUnPlato() {
		pedido.agregar(plato)
		assertTrue(pedido.tienePlato(plato))
	}
	
	@Test
	def unPedidoConUnEstadoDistintoAPreparandoLanzaUnaExcepcionAlAgregarUnPlato() {
		pedido.siguienteEstado()
		assertNotEquals(pedido.estado, EstadoPedidoPreparando)
		assertFalse(pedido.puedeAgregarPlato)
    	try {
			pedido.agregar(plato)
			fail("Se espera que falle por no poder agregar el plato") 
		}
		catch(UserException e) {
			assertEquals(e.getMessage(), "El pedido debe estar en preparación para poder agregar un plato")
		}
//		catch(NoPuedeAgregarPlatoException e) {
//			assertEquals(e.getMessage(), "El pedido debe estar en preparación para poder agregar un plato")
//		}	
	}
	
    @Test
    def unPedidoEnEstadoPreparandoConUnPlatoPermiteEliminarlo() {
    	pedido.agregar(plato)
    	assertTrue(pedido.tienePlato(plato))
    	pedido.eliminar(plato)
    	assertFalse(pedido.tienePlato(plato))
    }
    
    @Test
    def unPedidoDevuelveElNombreDelClienteAlQuePertenece() {
    	assertEquals(pedido.cliente.nombre, "Rodolfo")
    }
    
    @Test
    def unPedidoQueNoEsParaDeliveryTieneUnCostoDeDeliveryDeCero() {
    	assertFalse(pedido.delivery)
    	assertEquals(pedido.costoDelivery, 0)
    }
    
    @Test
    def unPedidoQueEsParaDeliveryTieneUnCostoDeDeliveryDe15() {
    	pedido.toggleDelivery
    	assertTrue(pedido.delivery)
    	assertEquals(pedido.costoDelivery, 15)

    }
    
    @Test
    def unPedidoQueNoTienePlatosAgregadosYNoEsParaDeliveryTieneUnMontoTotalDeCero() {
        assertFalse(pedido.delivery)
    	assertTrue(pedido.montoTotal == 0.00)	
    
    }
    
    @Test
    def unPedidoQueEsParaDeliveryYNoTienePlatosAgregadosTieneUnCostoDeDelivery() {
    	pedido.toggleDelivery
    	assertTrue(pedido.delivery)
    	assertTrue(pedido.montoTotal == 15.00)	
    }
     
    @Test
    def unPedidoQueTienePlatoAgregadoYNoEsParaDeliveryTieneUnMontoTotalIgualAlDePlatoAgregado() {
        assertFalse(pedido.delivery)
        when(plato.precio).thenReturn(10.00)
        pedido.agregar(plato)
    	assertTrue(pedido.montoTotal == 10.00)	
    }

    @Test
    def unPedidoQueTienePlatoAgregadoYEsParaDeliveryTieneUnMontoTotalIgualAlDePlatoAgregadoMas15() {
    	pedido.toggleDelivery
        assertTrue(pedido.delivery)
        when(plato.precio).thenReturn(15.00)
        pedido.agregar(plato)
    	assertTrue(pedido.montoTotal == 30.00)	
    }

    @Test
    def unPedidoTieneUnaFechaDeCreacionQuePermiteSerConsultada() {
    	assertEquals(pedido.fecha.class, LocalDateTime)
    }
    
    @Test
    def unPedidoTieneUnTiempoDeEsperaCalculadoSoloSiFueEntregado() {
    	assertNull(pedido.tiempoDeEspera)
    	pedido.siguienteEstado
    	assertNull(pedido.tiempoDeEspera)
		pedido.siguienteEstado
    	assertNotNull(pedido.tiempoDeEspera)
    	assertTrue(pedido.tiempoDeEspera.class == Long)
    }
	
}