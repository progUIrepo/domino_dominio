import org.junit.Test

import static org.junit.Assert.*
import org.junit.Before
import java.util.List
import domain.Plato
import domain.Ingrediente
import domain.Pizza
import domain.IngredienteContenidoException
import org.uqbar.commons.model.exceptions.UserException

class PlatoTest {
	
	Plato plato
	Ingrediente jamon
	Ingrediente muzzarella
	List<Ingrediente> ingredientesBase = newArrayList
		
    @Before
    def void setUp() {
		plato = new Plato 
        muzzarella = new Ingrediente("Muzzarella", 10.00)		
	    jamon = new Ingrediente("Jamon", 10.00) 	
	    ingredientesBase.add(muzzarella)
		plato.pizza = new Pizza("Muzzarela", 75.00, ingredientesBase)
    }

	@Test
	def unPlatoTieneUnPrecioBaseNumericoPositivo() {
		assertTrue(plato.precioBase > 0)
	}
	
	@Test
	def unPlatoEsGrandePorDefault() {
		assertTrue(plato.esGrande)
	}
	
 	
	@Test
	def unPlatoPuedeSerSeteadaComoPorcion() {
        plato.setPorcion
		assertFalse(plato.esGrande)
		assertTrue(plato.esPorcion)
	}
	
	@Test
	def unPlatoPuedeSerSeteadaComoChica() {
        plato.setChica
		assertFalse(plato.esGrande)
		assertTrue(plato.esChica)
	}
	
	@Test
	def unPlatoPuedeSerSeteadaComoFamiliar() {
        plato.setFamiliar
		assertFalse(plato.esGrande)
		assertTrue(plato.esFamiliar)
	}
	
	@Test
	def unPlatoGrandeSinModificarCuestaSuPrecioBase() {
		assertTrue(plato.esGrande)
		assertTrue(plato.getPrecio() == plato.precioBase)
	}
	
	@Test
	def unPlatoPorcionSinModificarCuestaUnOctavoDeSuPrecioBase() {
		plato.setPorcion
		assertTrue(plato.esPorcion);
		assertTrue(plato.precio == (plato.precioBase*0.125))
	}

	@Test
	def unPlatoChicaSinModificarCuestaLaMitadDeSuPrecioBase() {
		plato.setChica
		assertTrue(plato.esChica);
		assertTrue(plato.precio == (plato.precioBase*0.5))
	}
	
	@Test
	def unPlatoFamiliarSinModificarCuestaunCuartoMasQueSuPrecioBase() {
		plato.setFamiliar
		assertTrue(plato.esFamiliar);
		assertTrue(plato.precio == (plato.precioBase*1.25))
	}
	
	@Test
	def unPlatoAgregaIngredienteToda(){
		assertFalse(plato.tieneIngrediente(jamon))
		plato.agregaIngredienteToda(jamon)
		assertTrue(plato.tieneIngrediente(jamon))
	}
	
	@Test
	def unPlatoAgregaIngredienteIzq(){
		assertFalse(plato.tieneIngrediente(jamon))
		plato.agregaIngredienteIzq(jamon)
		assertTrue(plato.tieneIngrediente(jamon))
		assertFalse(plato.tieneIngredienteDer(jamon))
		assertTrue(plato.tieneIngredienteIzq(jamon))
	}
	
	@Test
	def unPlatoAgregaIngredienteDer(){
		assertFalse(plato.tieneIngrediente(jamon))
		plato.agregaIngredienteDer(jamon)
		assertTrue(plato.tieneIngrediente(jamon))
		assertTrue(plato.tieneIngredienteDer(jamon))
		assertFalse(plato.tieneIngredienteIzq(jamon))
	}
	
	@Test
	def unPlatoGrandeConUnIngredienteDerCuestaLaSumaDeElPrecioBaseMasElPrecioDelIngrediente(){
		plato.agregaIngredienteDer(jamon)
		assertTrue(plato.precio == (plato.precioBase+jamon.precio))
	}
	
	@Test
	def unPlatoGrandeConUnIngredienteTodaCuestaLaSumaDeElPrecioBaseMasElDobleDelPrecioDelIngrediente(){
		plato.agregaIngredienteToda(jamon)
		assertTrue(plato.precio == (plato.precioBase+jamon.precio*2))
	}
	
	@Test
	def unPlatoNoPuedeAgregarUnIngredienteQueEstaContenidoEnSuPizza(){
		try {
			plato.agregaIngredienteToda(muzzarella);
			fail("Se espera que falle por contener el ingrediente duplicado")
		}
		catch(UserException e) {
			assertEquals(e.getMessage(), "El ingrediente ya esta contenido en la pizza")
		}
//		catch(IngredienteContenidoException e) {
//			assertEquals(e.getMessage(), "El ingrediente ya esta contenido en la pizza")
//		}
	}
	
}
