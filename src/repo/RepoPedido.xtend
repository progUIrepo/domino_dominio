package repo

import org.uqbar.commons.model.CollectionBasedRepo
import domain.Pedido
import org.uqbar.commons.model.annotations.Observable
import java.util.List
import domain.Cliente

@Observable
class RepoPedido extends CollectionBasedRepo<Pedido> {
	
	override protected getCriterio(Pedido arg0) {
		null
	}

	override createExample() {
		new Pedido(new Cliente)
	}

	override getEntityType() {
		typeof(Pedido)
	}

	def List<Pedido> search(){
		
	}

	def getAll() {
		allInstances.toList
	}

	def getAbiertos() {
		allInstances.filter[pedido | pedido.esAbierto ].toList
	}

	def getCerrados() {
		allInstances.filter[pedido | pedido.esCerrado ].toList
	}
}