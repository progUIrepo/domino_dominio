package repo

import org.uqbar.commons.model.CollectionBasedRepo
import domain.Ingrediente
import org.uqbar.commons.model.annotations.Observable

@Observable
class RepoIngrediente extends CollectionBasedRepo<Ingrediente> {

	def create(String nom, Double value) {
		this.create( new Ingrediente => [
			nombre = nom
			precio = value
		])
	}

	override protected getCriterio(Ingrediente arg0) {
		null
	}

	override createExample() {
		new Ingrediente("",0.0)
	}

	override getEntityType() {
		typeof(Ingrediente)
	}
}