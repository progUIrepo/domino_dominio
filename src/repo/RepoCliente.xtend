package repo

import domain.Cliente
import org.uqbar.commons.model.CollectionBasedRepo
import org.uqbar.commons.model.annotations.Observable
import org.uqbar.commons.model.exceptions.UserException

@Observable
class RepoCliente extends CollectionBasedRepo<Cliente> {
	
	
	def void create(String nom, String nk, String pass, String ml, String dir) {
		this.create(new Cliente => [
			nombre = nom
			nick = nk
			password = pass
			mail = ml
			direccion = dir
		])
	}

	override void validateCreate(Cliente cl) {
		val mail = cl.mail
		val nick = cl.nick
		
		if (allInstances.exists[ cliente | cliente.mail == mail ]) {
			throw new UserException("Ya existe un cliente cliente con el mail" + mail)
		} else {
			if (allInstances.exists[ cliente | cliente.nick == nick ]) {
				throw new UserException("Ya existe un cliente cliente con el nick" + nick)
			}
		}
	}
	
	override createExample() {
		new Cliente
	}
	
	override getEntityType() {
		typeof(Cliente)
	}
	
	override protected getCriterio(Cliente noimporta) {
		null
	}

	def Cliente searchCliente(String mailOnick){
		allInstances.findFirst[cl | cl.mail == mailOnick || cl.nick == mailOnick]
	}
}