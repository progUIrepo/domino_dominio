package repo

import domain.Ingrediente
import domain.Pizza
import java.util.List
import org.uqbar.commons.model.CollectionBasedRepo
import org.uqbar.commons.model.annotations.Observable

@Observable
class RepoPizza extends CollectionBasedRepo<Pizza> {

	def Pizza create(String nom, double pb, List<Ingrediente> l) {
		val pizza = new Pizza(nom, pb, l)
		this.create (pizza)
		pizza 
	}

	override createExample() {
		new Pizza("", 0.0, #[])
	}
	
	override getEntityType() {
		typeof(Pizza)
	}
	
	override protected getCriterio(Pizza noimporta) {
		null
	}

}