package repo

import org.uqbar.commons.model.CollectionBasedRepo
import domain.Plato
import org.uqbar.commons.model.annotations.Observable

@Observable
class RepoPlato extends CollectionBasedRepo<Plato>{
	
	override protected getCriterio(Plato arg0) {
		null
	}
	
	override createExample() {
		new Plato
	}
	
	override getEntityType() {
		typeof(Plato)
	}

}