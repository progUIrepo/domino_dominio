package domain

import java.lang.Exception

class NoSePuedeCambiarDeEstadoException extends Exception {
	
	new(String string) {
		super(string)
	}
	
}