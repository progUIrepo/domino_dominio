package domain

import org.eclipse.xtend.lib.annotations.Accessors
import java.util.List
import org.uqbar.commons.model.Entity
import org.uqbar.commons.model.annotations.Observable
import org.uqbar.commons.model.annotations.TransactionalAndObservable

@Accessors
@TransactionalAndObservable
class Pizza extends Entity implements Cloneable {
    String nombre
    double precioBase
    Boolean isPromo = false
    List<Ingrediente> ingredientesDer = newArrayList
    List<Ingrediente> ingredientesIzq = newArrayList
	
	new() {
		super()
	}
	
	new(String string, double d, List<Ingrediente> ingredientes) {
		this.nombre = string
		this.precioBase = d
		this.ingredientesDer = ingredientes
		this.ingredientesIzq = ingredientes
	}
	
	def tieneIngrediente(Ingrediente ingrediente){
		this.ingredientesDer.contains(ingrediente) || this.ingredientesIzq.contains(ingrediente);
	}
}