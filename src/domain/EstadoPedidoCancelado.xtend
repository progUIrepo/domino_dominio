package domain

import java.time.LocalDateTime
import org.uqbar.commons.model.exceptions.UserException

class EstadoPedidoCancelado extends EstadoPedido {
	
	override anterior(Boolean delivery) {
		throw new UserException("El pedido fue cancelado. Si lo desea puede hacer un nuevo pedido")
		//throw new NoSePuedeCambiarDeEstadoException("El pedido fue cancelado. Si lo desea puede hacer un nuevo pedido")
	}
	
	override cancelado() {
		throw new UserException("El pedido fue cancelado. Si lo desea puede hacer un nuevo pedido")
		//throw new NoSePuedeCambiarDeEstadoException("El pedido fue cancelado. Si lo desea puede hacer un nuevo pedido")
	}
	
	override puedeCambiarAnterior() {
		false
	}
	
	override puedeCambiarSiguiente() {
		false
	}
	
	override siguiente(Boolean delivery, LocalDateTime fecha) {
		throw new UserException("El pedido fue cancelado. Si lo desea puede hacer un nuevo pedido")
		//throw new NoSePuedeCambiarDeEstadoException("El pedido fue cancelado. Si lo desea puede hacer un nuevo pedido")
	}
	
	override toString() {
		"Cancelado"
	}
	
	override Boolean esAbierto() {
		false
	}
	
}
