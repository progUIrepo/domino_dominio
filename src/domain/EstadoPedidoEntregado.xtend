package domain

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import org.uqbar.commons.model.exceptions.UserException

class EstadoPedidoEntregado extends EstadoPedido {
	
	Long tiempoDeEspera
	
	new(java.time.LocalDateTime time) {
		this.tiempoDeEspera = ChronoUnit.MINUTES.between(time, LocalDateTime.now())
	}
	
	override siguiente(Boolean delivery, LocalDateTime fecha) {
		throw new UserException("El pedido fue entregado. Si lo desea puede hacer un nuevo pedido")
		//throw new NoSePuedeCambiarDeEstadoException("El pedido fue entregado. Si lo desea puede hacer un nuevo pedido")
	}
	
	override anterior(Boolean delivery) {
		throw new UserException("El pedido fue entregado. Si lo desea puede hacer un nuevo pedido")
		//throw new NoSePuedeCambiarDeEstadoException("El pedido fue entregado. Si lo desea puede hacer un nuevo pedido")
	}
	
	override cancelado() {
		throw new UserException("El pedido fue entregado. Si lo desea puede hacer un nuevo pedido")
		//throw new NoSePuedeCambiarDeEstadoException("El pedido fue entregado. Si lo desea puede hacer un nuevo pedido")
	}
	
	override puedeCambiarAnterior() {
		false
	}
	
	override puedeCambiarSiguiente() {
		false
	}
	
	override tiempoDeEspera() {
		this.tiempoDeEspera
	}
	
	override toString() {
		"Entregado"
	}
	
	override Boolean esAbierto() {
		false	
	}	
}