package domain

import java.lang.Exception

class NoPuedeAgregarPlatoException extends Exception {
	new(String string) {
		super(string)
	}
	
}