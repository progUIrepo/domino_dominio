package domain

import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.Entity
import org.uqbar.commons.model.annotations.TransactionalAndObservable

@Accessors
@TransactionalAndObservable
class Cliente extends Entity implements Cloneable {
	String nombre
	String nick
	String password
	String mail
	String direccion
}