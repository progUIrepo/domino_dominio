package domain

import java.time.LocalDateTime

class EstadoPedidoListoParaEnviar extends EstadoPedido {
	
	override siguiente(Boolean delivery, LocalDateTime fecha) {
		new EstadoPedidoEnViaje
	}
	
	override anterior(Boolean delivery) {
		new EstadoPedidoPreparando
	}
	
	override toString() {
		"Listo para enviar"
	}
	
}