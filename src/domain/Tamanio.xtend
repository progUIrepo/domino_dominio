package domain

abstract class Tamanio {
	
	def Boolean esGrande(){
		return false
	}
	
	def Boolean esPorcion(){
		return false
	}
	
	def Boolean esChica(){
		return false
	}
	
	def Boolean esFamiliar() {
		return false
	}
	
	def double getPrecio(Plato pizza) {
		return pizza.precioBase
	}
	override toString()
	
}
class TamanioGrande extends Tamanio {
    
    override Boolean esGrande(){
    	return true
    }
				
	override toString() {
		"Grande"
	}
    
}

class TamanioPorcion extends Tamanio {
	
	override Boolean esPorcion(){
		return true
	}
	
	override getPrecio(Plato pizza) {
		return super.getPrecio(pizza) * 0.125; 
	}
	override toString() {
		"Porcion"
	}
	
}

class TamanioChica extends Tamanio {
	 
	override Boolean esChica(){
		return true
	}
	
	override getPrecio(Plato pizza) {
		return super.getPrecio(pizza) * 0.5;
	}
	override toString() {
		"Chica"
	}
}

class TamanioFamiliar extends Tamanio {
	
	override Boolean esFamiliar(){
		return true
	}
	
	override getPrecio(Plato pizza) {
		return super.getPrecio(pizza) * 1.25;
	}
	override toString() {
		"Familiar"
	}	
}