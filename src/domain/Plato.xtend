package domain

import org.eclipse.xtend.lib.annotations.Accessors
import java.util.List
import org.uqbar.commons.model.Entity
import org.uqbar.commons.model.annotations.TransactionalAndObservable
import org.uqbar.commons.model.exceptions.UserException

@Accessors
@TransactionalAndObservable
class Plato extends Entity implements Cloneable {
	Pizza pizza
	List<Ingrediente> ingredientesIzq = newArrayList
	List<Ingrediente> ingredientesDer = newArrayList
	Tamanio tamanio = new TamanioGrande
	
	def esGrande() {
		this.tamanio.esGrande
	}
	
	def esPorcion() {
		this.tamanio.esPorcion
	}
	
	def esChica() {
		this.tamanio.esChica
	}
	
	def esFamiliar() {
		this.tamanio.esFamiliar
	}
	
	def setPorcion() {
		this.tamanio = new TamanioPorcion
	}
	
	def setChica() {
		this.tamanio = new TamanioChica
	}
	
	def setFamiliar() {
		this.tamanio = new TamanioFamiliar
	}
	
//	def getTamanio() {
//		this.tamanio
//		//new TamanioGrande
//	} LO SAQUE ME PARECE QUE QUEDO DE MAS EN ALGUN MOMENTO
	
	def getPrecio() {
		this.tamanio.getPrecio(this) + this.sumarPrecioIngredientes
	}
	
	def sumarPrecioIngredientes() {
		this.sumarPrecioTodos(ingredientesDer) + this.sumarPrecioTodos(ingredientesIzq);
	}
	
	def sumarPrecioTodos(List<Ingrediente> ingredientes) {
		var sum =  0.0
		for(Ingrediente ingrediente : ingredientes) {
		    sum += ingrediente.precio	
		}
		sum
	}
	
	def agregaIngredienteToda(Ingrediente ingrediente) {
		if (this.pizza.tieneIngrediente(ingrediente)) {
			throw new UserException("El ingrediente ya esta contenido en la pizza")
		}
        this.ingredientesDer.add(ingrediente)
        this.ingredientesIzq.add(ingrediente) 
	}
	
	def tieneIngrediente(Ingrediente ingrediente) {
		tieneIngredienteDer(ingrediente) || tieneIngredienteIzq(ingrediente)
	}
	
	def agregaIngredienteIzq(Ingrediente ingrediente) {
		this.ingredientesIzq.add(ingrediente)
	}
	
	def agregaIngredienteDer(Ingrediente ingrediente) {
		this.ingredientesDer.add(ingrediente)
	}
	
	def tieneIngredienteDer(Ingrediente ingrediente) {
		this.ingredientesDer.contains(ingrediente)
	}
	
	def tieneIngredienteIzq(Ingrediente ingrediente) {
		this.ingredientesIzq.contains(ingrediente)
	}
	
	def getPrecioBase() {
		this.pizza.precioBase
	}
	
	
}

