package domain

import java.time.LocalDateTime

class EstadoPedidoListoParaRetirar extends EstadoPedido {
	
	override siguiente(Boolean delivery, LocalDateTime fecha) {
		new EstadoPedidoEntregado(fecha) 
	}
	
	override anterior(Boolean delivery) {
		new EstadoPedidoPreparando
	}

	override toString() {
		"Listo para retirar"
	}
}