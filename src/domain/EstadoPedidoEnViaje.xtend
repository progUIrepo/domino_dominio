package domain

import java.time.LocalDateTime

class EstadoPedidoEnViaje extends EstadoPedido {
	
	override siguiente(Boolean delivery, LocalDateTime fecha) {
		new EstadoPedidoEntregado(fecha)
	}
	
	override anterior(Boolean delivery) {
		new EstadoPedidoListoParaEnviar
	}
	
	override toString() {
		"En viaje"
	}
	
}