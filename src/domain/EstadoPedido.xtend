package domain

import java.time.LocalDateTime
import org.uqbar.commons.model.annotations.Observable

@Observable
abstract class EstadoPedido {
	def EstadoPedido siguiente(Boolean delivery, LocalDateTime fecha) 
	def EstadoPedido anterior(Boolean delivery)

	override def String toString()

	def EstadoPedido cancelado() {
		new EstadoPedidoCancelado
	}

	def Boolean puedeCambiarSiguiente() {
		true
	}

	def Boolean puedeCambiarAnterior() {
		true
	}
	
	def Boolean puedeAgregarPlato() {
		false
	}
 
    // TODO: Cambiarlo para que lance una excepción y testearlo	
	def Boolean toggleDelivery(Boolean delivery) {
		delivery
	}
	
	def Long tiempoDeEspera() {
		null
	}
    	
	def Boolean esAbierto() {
		true
	}

	
}

