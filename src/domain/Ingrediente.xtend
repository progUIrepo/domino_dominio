package domain

import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.Entity
import org.uqbar.commons.model.annotations.TransactionalAndObservable

@Accessors
@TransactionalAndObservable
class Ingrediente extends Entity implements Cloneable {
	String nombre
	double precio

	new() {
	}

	new(String nombre, Double precio){
		this.nombre = nombre
		this.precio = precio
	}
}