package domain

import java.time.LocalDateTime
import org.uqbar.commons.model.exceptions.UserException

class EstadoPedidoPreparando extends EstadoPedido{
	
	override siguiente(Boolean delivery, LocalDateTime fecha) {
		if (delivery) {
			new EstadoPedidoListoParaEnviar
		} else {
			new EstadoPedidoListoParaRetirar
		}
	}
	
	override anterior(Boolean delivery) {
		throw new UserException("Un pedido que se esta preparando no tiene un estado anterior")
		//throw new NoSePuedeCambiarDeEstadoException("Un pedido que se esta preparando no tiene un estado anterior")
	}
	
	override puedeCambiarAnterior() {
		false
	}
	
	override puedeAgregarPlato() {
		true
	}
	
	override toggleDelivery(Boolean delivery) {
		(! delivery)
	}
	
	override toString() {
		"Preparando"
	}
	
	
}
