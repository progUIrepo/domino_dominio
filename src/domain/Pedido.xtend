package domain

import org.eclipse.xtend.lib.annotations.Accessors
import java.util.List
import java.time.LocalDateTime
import org.uqbar.commons.model.annotations.TransactionalAndObservable
import org.uqbar.commons.model.Entity
import org.uqbar.commons.model.exceptions.UserException
import org.uqbar.commons.model.annotations.Dependencies
import org.uqbar.commons.model.utils.ObservableUtils

@Accessors
@TransactionalAndObservable
class Pedido extends Entity implements Cloneable {
    
    EstadoPedido estado = new EstadoPedidoPreparando
    Boolean delivery = false	
	String aclaraciones
	List<Plato> platos = newArrayList
	Cliente cliente
	LocalDateTime fecha = LocalDateTime.now()
	
	
	new(Cliente cliente) {
		this.cliente = cliente
	}
	
	def Boolean puedeAgregarPlato() {
		this.estado.puedeAgregarPlato
	}
	
	def agregar(Plato plato) {
		if (this.puedeAgregarPlato) {
			this.platos.add(plato)
		} else {
			throw new UserException("El pedido debe estar en preparación para poder agregar un plato")
		}
	}
	
	def tienePlato(Plato plato) {
		this.platos.contains(plato)
	}
	
	def esDelivery() {
		delivery
	}
	
	def toggleDelivery() {
		this.delivery = this.estado.toggleDelivery(delivery)
	}
	

	def eliminar(Plato plato) {
		platos.remove(plato)
	}
	
	def siguienteEstado() {
		this.estado = this.estado.siguiente(this.delivery, this.fecha)
	}
	
	def void cancelar() {
		this.estado = this.estado.cancelado
	}
	
	def anteriorEstado() {
		this.estado = this.estado.anterior(this.delivery)
	}
	def setDelivery(Boolean b){
		this.delivery = b
		ObservableUtils.firePropertyChanged(this, "costoDelivery")
	}
	// este if hay que removerlo usando delegación a un ojetoc polifomicoc que se autoevalúe
	def costoDelivery() {
		if (delivery) {
			return 15
		} else {
			return 0
		}
	}
	@Dependencies("costoDelivery","sumarMontosPlatos")
	def getMontoTotal() {
		this.costoDelivery + this.sumarMontosPlatos
	}
	
	@Dependencies("montoTotal")
	def getPrecio() {
		getMontoTotal()
	}
	
	@Dependencies("platos")
	def getSumarMontosPlatos() {
		var suma = 0.00
		for(Plato plato : this.platos) {
			suma = suma + plato.precio
		}
		return suma
	}
	
	def tiempoDeEspera() {
		this.estado.tiempoDeEspera
	}

	def getPedidoAbierto() {
		this.estado.puedeCambiarSiguiente
	}

	def esAbierto() {
		this.estado.esAbierto
	}
	
	def esCerrado() {
		!this.esAbierto
	}
	
}
