package appModel

import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.annotations.TransactionalAndObservable
import java.io.Serializable

import domain.Pizza
import domain.Ingrediente
import java.util.List
import repo.RepoIngrediente
import org.uqbar.commons.applicationContext.ApplicationContext
import repo.RepoPizza

@Accessors
@TransactionalAndObservable
class MenuAppModel implements Serializable{

	List<Pizza> promos
	Pizza promoSeleccionada
	List<Ingrediente> ingredientes
	Ingrediente ingredienteSeleccionado

	def void crearPromo() {
		val current = new Pizza("", 0.0, #[])
		promos.add(current)
		promoSeleccionada = current
		this.editarPromo()
	}
	
	def editarPromo() {
		this.promos.remove(promoSeleccionada)
		this.promos.add(promoSeleccionada)
	}
	
	def eliminarPromo(Pizza pizza) {
		this.promos.remove(pizza)
    }
    	
	def void crearIngrediente() {
		val current = new Ingrediente("", 0.0)
		ingredientes.add(current)
		ingredienteSeleccionado = current
		this.editarIngrediente(ingredienteSeleccionado)
	}
	
	def editarIngrediente(Ingrediente ingrediente) {
		this.ingredientes.remove(ingrediente)
		this.ingredientes.add(ingrediente)
	}
	
	def eliminarIngrediente(Ingrediente ingrediente) {
		this.ingredientes.remove(ingrediente)
	}
	
	def RepoPizza getRepoPromos() {
		ApplicationContext.instance.getSingleton(typeof(Pizza))
	}
	
	def RepoIngrediente getRepoIngredientes() {
		ApplicationContext.instance.getSingleton(typeof(Ingrediente))
	}
	
	def void getPromos() {
		promos = getRepoPromos.allInstances.toList
	}
	
	def void getIngredientes() {
		ingredientes = getRepoIngredientes.allInstances.toList
	}
}