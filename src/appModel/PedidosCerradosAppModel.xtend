package appModel

import domain.Pedido
import java.util.List
import repo.RepoPedido
import org.uqbar.commons.applicationContext.ApplicationContext
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.annotations.TransactionalAndObservable
import org.uqbar.commons.model.annotations.Dependencies

import java.io.Serializable
@Accessors
@TransactionalAndObservable
class PedidosCerradosAppModel implements Serializable {
	
	Pedido pedidoSeleccionado
	List<Pedido> pedidosCerrados
	
	@Dependencies("getPedidosCerrados")
	def void getCerrados() {
		this.pedidosCerrados = repoPedidos.cerrados
	}
	
	def RepoPedido getRepoPedidos() {
		ApplicationContext.instance.getSingleton(typeof(Pedido))
	}
}