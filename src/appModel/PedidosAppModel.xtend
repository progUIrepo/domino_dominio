package appModel

import org.eclipse.xtend.lib.annotations.Accessors
import java.io.Serializable
import domain.Pedido
import java.util.List
import repo.RepoPedido
import org.uqbar.commons.applicationContext.ApplicationContext
import domain.Menu
import org.uqbar.commons.model.annotations.Dependencies
import org.uqbar.commons.model.annotations.TransactionalAndObservable

@Accessors
@TransactionalAndObservable
class PedidosAppModel implements Serializable{
	Pedido pedidoSeleccionado
	Menu menu
	List<Pedido> pedidos

	@Dependencies("getPedidosAbiertos")
	def void getAbiertos() {
		this.pedidos = repoPedidos.abiertos
	}

	def void cancelarPedido(){
		pedidoSeleccionado.cancelar
	}

	def RepoPedido getRepoPedidos() {
		ApplicationContext.instance.getSingleton(typeof(Pedido))
	}
	
}