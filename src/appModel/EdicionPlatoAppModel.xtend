package appModel

import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.annotations.TransactionalAndObservable
import java.io.Serializable
import java.util.List
import domain.Pizza
import repo.RepoPizza
import org.uqbar.commons.applicationContext.ApplicationContext
import repo.RepoIngrediente
import domain.Ingrediente
import domain.Plato

@Accessors
@TransactionalAndObservable
class EdicionPlatoAppModel implements Serializable {
	
	List<Pizza> pizzas
	List<Ingrediente> ingredientes
	List<String>tams = #["grande","chica", "porcion", "familiar"]
	Pizza pizzaSeleccionada
	List<Ingrediente> agregados
	Plato platoseleccionado
	
	new(Plato plato){
		platoseleccionado = plato
	}
		
	def RepoPizza getRepoPromos() {
		ApplicationContext.instance.getSingleton(typeof(Pizza))
	}
	
	def RepoIngrediente getRepoIngredientes() {
		ApplicationContext.instance.getSingleton(typeof(Ingrediente))
	}
	
	def void getPromos() {
		pizzas = getRepoPromos.allInstances.toList
	}
	
	def List<Ingrediente> getIngredientes() {
		ingredientes = getRepoIngredientes.allInstances.toList
	}
}